#!/bin/bash

battery=`acpi | cut -d " " -f 4-7`
date=`date +"%H:%M %d/%m/%y"`
temp=`acpi -t | cut -d " " -f 4-4 | sed '1d'`" C"

echo "$battery | $temp | $date "
