#!/bin/bash
pac=$(checkupdates | wc -l)
aur=$(cower -u | wc -l)

check=$((pac + aur))
if [[ "$check" == "0" ]]
then
	echo "up to date!"
fi

if [[ "$check" > "1" ]]
then
    echo "$check updates"
else
    echo "$check update"
fi


